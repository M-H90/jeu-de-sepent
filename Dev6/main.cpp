/*
 * fichier : main.cpp
 * auteur.e : Vincent Ducharme
 * date : 2016
 * modifications :
 * 01/11/2019 Marie-Flavie Auclair-Fortier : refactoring du code
 * description : Ce fichier contient un jeu de serpent
 */

#include "jeu.h"

#include <iostream>
#include <limits>
#include <fstream>

using namespace std;

int main()
{
    // Prototype des fonctions locales
    int lireEntier(const string &);

    // Declaration des variables locales
    int nbLignes;       // nombre de lignes dans le jeu
    int nbColonnes;     // nombre de colonnes dans le jeu

    ifstream fichierTest;   // flux d'entree de type fichier

    // workaround pour avoir une entree redirigee dans le fichier de test
    fichierTest.open("test1");  // ouvre le fichier
    
    // redirige le flux standard vers le fichier, le cin pourra etre utilise normalement
    // ****** a mettre en commentaires cette ligne pour remettre le fonctionnement a normal
//    cin.rdbuf(fichierTest.rdbuf());
        
    // 1. Lire la taille de la grille de jeu
    // 1.1 Lire le nombre de lignes
    nbLignes = lireEntier("Entrez le nombre de lignes de la grille de jeu : ");

    // 1.2 Lire le nombre de colonnes
    nbColonnes = lireEntier("Entrez le nombre de colonnes de la grille de jeu : ");
    
    // 5. Demarrer le jeu (module Jouer)
    jouer(nbLignes, nbColonnes);
    
    return 0;
}

/*
* description : lit un entier jusqu'à la lecture d'un entier
* entrées :
*  i_precisions, un message de précision sur ce qui est demandé
* retour : nombre lu
* post : le nombre lu est un entier
* précisions : la fonction est robuste
*/

int lireEntier(const string &i_precisions) {
    
    // Declaration des variables locales
    int nombreLu;       // nombre de lignes dans le jeu

    // 1. Afficher le message de precisions
    cout << i_precisions;
    
    // 2. Tant qu'on n'a pas lu un entier
    while (!(cin >> nombreLu))
    {
        // 2.1 Afficher un message d'erreur et redemander le nombre de ligne
        cout << "Vous devez entrer un nombre entier!" << endl<< i_precisions;
        // 2.2 Vider le tampon de lecture
        cin.clear();
        // 2.3 Ignorer tous les caracteres deja entres
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
    
    return nombreLu;
}
