//
//  fonctions.cpp
//  D6_Snake
//
//  Created by Marie-Flavie Auclair-Fortier on 2019-11-05.
//Copyright © 2019 Marie-Flavie Auclair-Fortier. All rights reserved.
//


#include "fonctions.h"

const unsigned char POMME_DROITE = ')';
const unsigned char POMME_GAUCHE = '(';
const unsigned char LIBRE = ' ';
#if defined(_WIN32)
const unsigned char MUR = '/';
const unsigned char SERPENT = '$';
#else
#include <string>
const unsigned char MUR = '/';
const unsigned char SERPENT = '$';

#endif

/********************************/

/*
 * description : Fonction qui cree une carte
 * entrees : Nombres de lignes et le nombres de colonnes
 * sorties : carteDuJeu
 * pre : Le nombres de lignes et de colonnes doivent etre plus grands que 0.
 * post : La carte a les dimensions demandees
 * precisions : La fonction fait apparaitre la pomme une premiere fois
 */

Carte creerCarte(int nbLignes, int nbColonnes)
{
	Carte carteDuJeu;
	carteDuJeu.m_nNbLig = nbLignes;
	carteDuJeu.m_nNbCol = nbColonnes;

	carteDuJeu.carte.resize(carteDuJeu.m_nNbLig + 2, vector<char>(carteDuJeu.m_nNbCol * 2 + 2, VIDE));

	for (int ligne = 0; ligne < carteDuJeu.carte.size(); ligne++)
	{
		for (int colonne = 0; colonne < carteDuJeu.carte[ligne].size(); colonne++)
		{


			if (colonne == 0 || colonne == carteDuJeu.carte[ligne].size() - 1 || ligne == 0 || ligne == carteDuJeu.carte.size() - 1)
			{
				carteDuJeu.carte[ligne][colonne] = MUR;
			}
			else
			{
				carteDuJeu.carte[ligne][colonne] = LIBRE;
			}

		}
	}

	creerPomme(carteDuJeu);

	return carteDuJeu;
}

/*
 * description : Fonction qui cree un serpent
 * entrees : Le nombre de lignes dans le tableau du jeu et Le nombres de colonnes dans le tableau du jeu
 * sorties : Serpent
 * pre : Le nombres de lignes et de colonnes doivent être plus grands que 0.
 * post : Le serpent a une taille de 3 et la position correspond au milieu de la carte de jeu.
 * precisions :
 */

Serpent creerSerpent(int nbLignes, int  nbColonnes)
{
	Serpent Ssssserpent;
	Coordonnee initial;
	initial.positionY = nbLignes;
	initial.positionX = nbColonnes * 2;
	Ssssserpent.taille = TAILLE_INITIALE;
	Ssssserpent.serpent.resize(TAILLE_INITIALE, initial);

	return Ssssserpent;
}

/*
 * description : Fonction qui cree la pomme du jeu
 * entrees : La carte
 * sorties : Une position pour la pomme
 * retour : aucun
 * pre : La carte dois etre valide
 * post : La pomme doit être dans la carte
 * precisions : Si c'est du vrai aleatoire il y a une chance que la premiere pomme apparaisse sur le serpent
 */

void creerPomme(Carte& i_carte)
{
	Coordonnee fruit;

	do
	{
		fruit.positionY = genererValeur(1, i_carte.m_nNbLig - 1);
		fruit.positionX = genererValeur(1, i_carte.m_nNbCol * 2 - 1);
	} while (i_carte.carte[fruit.positionY][fruit.positionX - 1] != LIBRE &&
		i_carte.carte[fruit.positionY][fruit.positionX + 1] != LIBRE &&
		i_carte.carte[fruit.positionY][fruit.positionX] != LIBRE);

	if (fruit.positionX % 2 == 0)
	{
		i_carte.carte[fruit.positionY][fruit.positionX - 1] = POMME_GAUCHE;
		i_carte.carte[fruit.positionY][fruit.positionX] = POMME_DROITE;
	}
	else
	{
		i_carte.carte[fruit.positionY][fruit.positionX] = POMME_GAUCHE;
		i_carte.carte[fruit.positionY][fruit.positionX + 1] = POMME_DROITE;
	}
}

/*
 * description : Fonction qui assigne le serpent dans la carte
 * entrees : La carte du jeu et le serpent
 * sorties : Carte du jeu avec le serpent dedans
 * pre : La carte dois etre valide
 * post : Le serpent doit etre au milieu de la carte
 * precisions :
 */

void assignerSerpent(Carte& carteDuJeu, Serpent& Ssssserpent)
{
	//carteDuJeu.carte[Ssssserpent.positionX][Ssssserpent.positionY] = Ssssserpent.taille;

	for (int coordonne = 0; coordonne < Ssssserpent.serpent.size(); coordonne++)
	{
		carteDuJeu.carte[Ssssserpent.serpent[coordonne].positionY][Ssssserpent.serpent[coordonne].positionX] = SERPENT;
		carteDuJeu.carte[Ssssserpent.serpent[coordonne].positionY][Ssssserpent.serpent[coordonne].positionX - 1] = SERPENT;
	}
}

/*
 * description : Surcharge d'operateur pour afficher la carte
 * entrees : La carte
 * sorties : affichage de la carte
 * retour : L'ostream
 * pre : La carte dois etre valide
 * post : Dois afficher une carte
 * precisions :
 */

ostream& operator << (ostream& os, const Carte& obj)
{

	vector<vector<char>> v2D = obj.carte;

	for (int ligne = 0; ligne < v2D.size(); ligne++)
	{
		vector<char> curLigne = v2D[ligne];
		for (int colonne = 0; colonne < curLigne.size(); colonne++)
		{
			char element = curLigne[colonne];
			os << element;
		}
		os << endl;
	}

	return os;
}

/*
 * description : Fonction qui transforme un caractere en direction pour le serpent
 * entrees : Le serpent et la direction
 * sorties : La direction que le seprent dois prendre
 * retour : Les directions
 * pre : Le serpent dois etre valide et les caracteres doivent etre ( q, w, a, s, d, )
 * post : Le serpent dois aller dans la direction demande
 * precisions :
 */

Direction getDirectionSuivante(char cDirection, Serpent& io_sssserpent)
{

	switch (cDirection)
	{
	case'a':
		//cDirection = Direction::GAUCHE;
		if (io_sssserpent.ancDirection == Direction::DROITE)
		{
			return Direction::DROITE;
		}
		io_sssserpent.ancDirection = Direction::GAUCHE;
		return Direction::GAUCHE;
		break;
	case'd':
		//cDirection = Direction::DROITE;
		if (io_sssserpent.ancDirection == Direction::GAUCHE)
		{
			return Direction::GAUCHE;
		}
		io_sssserpent.ancDirection = Direction::DROITE;
		return Direction::DROITE;
		break;
	case'w':
		//cDirection = Direction::HAUT;
		if (io_sssserpent.ancDirection == Direction::BAS)
		{
			return Direction::BAS;
		}
		io_sssserpent.ancDirection = Direction::HAUT;
		return Direction::HAUT;
		break;
	case's':
		//cDirection = Direction::BAS;
		if (io_sssserpent.ancDirection == Direction::HAUT)
		{
			return Direction::HAUT;
		}
		io_sssserpent.ancDirection = Direction::BAS;
		return Direction::BAS;
		break;
	}
}

/*
 * description : Fonction qui execute la direction demande dans getDirectionSuivante() et qui verifie si la position est valide
 * entrees : Une direction, la carte et le serpent
 * sorties : Le deplacement du serpent
 * retour : boolean
 * pre : La driection, la carte et le serpent doivent etre valide
 * post : Le serpent doit se deplacer dans la direction demande
 * precisions : Fait apparaitre les pommes apres que la premiere a ete mange
 */

bool deplacer(Direction dirSuivante, Carte& io_carte, Serpent& io_sssserpent)
{
	Coordonnee initial;

	if (dirSuivante == Direction::HAUT)
	{
		initial.positionX = io_sssserpent.serpent[io_sssserpent.serpent.size() - 1].positionX;
		initial.positionY = io_sssserpent.serpent[io_sssserpent.serpent.size() - 1].positionY - 1;
	}
	else if (dirSuivante == Direction::BAS)
	{
		initial.positionX = io_sssserpent.serpent[io_sssserpent.serpent.size() - 1].positionX;
		initial.positionY = io_sssserpent.serpent[io_sssserpent.serpent.size() - 1].positionY + 1;
	}

	else if (dirSuivante == Direction::GAUCHE)
	{
		initial.positionX = io_sssserpent.serpent[io_sssserpent.serpent.size() - 1].positionX - 2;
		initial.positionY = io_sssserpent.serpent[io_sssserpent.serpent.size() - 1].positionY;
	}
	else if (dirSuivante == Direction::DROITE)
	{
		initial.positionX = io_sssserpent.serpent[io_sssserpent.serpent.size() - 1].positionX + 2;
		initial.positionY = io_sssserpent.serpent[io_sssserpent.serpent.size() - 1].positionY;
	}

	if (io_carte.carte[initial.positionY][initial.positionX] == MUR || io_carte.carte[initial.positionY][initial.positionX] == SERPENT)
	{
		return false;
	}

	if (io_carte.carte[initial.positionY][initial.positionX] == POMME_DROITE || io_carte.carte[initial.positionY][initial.positionX] == POMME_GAUCHE)
	{
		creerPomme(io_carte);

		io_sssserpent.taille++;
	}
	else
	{
		for (int coordonne = 0; coordonne < io_sssserpent.serpent.size(); coordonne++)
		{
			io_carte.carte[io_sssserpent.serpent[coordonne].positionY][io_sssserpent.serpent[coordonne].positionX] = LIBRE;
			io_carte.carte[io_sssserpent.serpent[coordonne].positionY][io_sssserpent.serpent[coordonne].positionX - 1] = LIBRE;
		}
		io_sssserpent.serpent.erase(io_sssserpent.serpent.begin());
	}

	io_sssserpent.serpent.push_back(initial);

	return true;
}


/*
 * description : Fonction qui met a jour le serpent dans la carte
 * entrees : La carte et le serpent
 * sorties : La nouvelle position du serpent dans la carte
 * retour : Le serpent
 * pre : La carte et le serpent doivent etre valide
 * post : La position du serpent doit etre valide
 * precisions :
 */

Serpent mettreAJour(Serpent& io_sssserpent, Carte& io_carte)
{
	assignerSerpent(io_carte, io_sssserpent);

	return io_sssserpent;
}


/*
 * description : Fonction qui donne le score du jeu selon la taille du serpent
 * entrees : Le serpent
 * sorties : La taille du serpent
 * retour : La taille du serpent
 * pre : La taille du serpent dois etre valide
 * post : La taille dois etre la taille complete du serpent
 * precisions :
 */

int getLongueur(Serpent& io_sssserpent)
{

	return io_sssserpent.taille;
}