//
//  types.h
//  D6_Snake
//
//  Created by Marie-Flavie Auclair-Fortier on 2019-11-05.
//  Copyright © 2019 Marie-Flavie Auclair-Fortier. All rights reserved.
//

#include "utilitaires.h"
#include <iostream>
#include <string>
#include <vector>
#include <cassert>

using namespace std;

#ifndef types_h
#define types_h
// Définissez vos types ici


// Enumeration pour les directions du serpent
enum class Direction { GAUCHE, DROITE, HAUT, BAS };

// Structure pour creer la carte du jeu
struct Carte
{
	int m_nNbLig;
	int m_nNbCol;
	vector<vector<char>> carte;
};

/*
 * description : Surcharge d'operateur pour afficher la carte
 * entrées : La carte
 * sorties : affichage de la carte
 * retour : L'ostream
 * pré : La carte dois être valide
 * post : Dois afficher une carte
 * précisions :
 */
ostream& operator << (ostream& os, const Carte& obj);

// Structure pour les coordonne du serpent
struct Coordonnee
{
	int positionX;
	int positionY;
};

// Structure pour creer le serpent
struct Serpent
{
	Direction ancDirection;
	int taille;
	vector<Coordonnee> serpent;
};



#endif /* types_h */d