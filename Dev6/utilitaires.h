/*
* fichier : utilitaire.h
* auteur.e : Vincent Ducharme
* date : 2016
* modifications :
* 01/11/2019 Marie-Flavie Auclair-Fortier : refactoring du code
* description : Ce fichier contient les definitions de fonctions utilitaires pour l'affichage en couleur et la generation de nombres aleatoires
*/

#ifndef _UTILITAIRES_H_
#define _UTILITAIRES_H_

#define _CONSOLE_COLOR_ // enlever le commentaire pour fonctionner en couleur (optionnel)

#include <chrono>
#include <random>

#if defined(_WIN32)
    #include <Windows.h>
    #ifdef max
        #undef max
    #endif
#endif

enum class Color
{
	BleuFonce = 1,
	Rouge = 12,
	Vert = 2,
	Blanc = 15,
};

class Console
{
public:
    /*
     * description : Efface completement l'ecran
     * entrées : aucune
     * sorties : aucune
     * retour : aucun
     * précisions : Cette fonction contient du code specifique au systeme d'exploitation utilise.
     */
    static void ClearScreen()
    {
#if defined(_CONSOLE_COLOR_)
#if defined(_WIN32)
        std::system("cls");
#else // not _WIN32
        // Assume POSIX
        std::system("clear");
#endif //_WIN32
#endif //_CONSOLE_COLOR_

    }

   /*
    * description : Modifie la couleur d'ecriture de la console de sortie parmi un choix (enumeration Color)
    * entrées :
      c, Couleur a utiliser
    * sorties : aucune
    * retour : aucun
    * précisions : Cette fonction contient du code specifique au systeme d'exploitation utilise.
    */

    static void SetConsoleColor(Color c)
    {
#if defined(_CONSOLE_COLOR_)
#if defined(_WIN32)
        HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
        SetConsoleTextAttribute(hConsole, (int)c);
#else // not _WIN32
        std::string color = "\033[0m";
        switch (c)
        {
        case BleuFonce:
            color = "\033[2;34m";
            break;
        case Rouge:
            color = "\033[0;31m";
            break;
		case Vert:
			color = "\033[0;32m";
			break;
		case Blanc:
            color = "\033[0;37m";
            break;
        }
        cout << color;
#endif //_WIN32
#endif //_CONSOLE_COLOR_
    }
};

/*
 * description : génère une valeur aléatoire entière entre deux bornes
 * entrées :
   i_min valeur minimale que peut prendre la valeur
   i_max valeur max que peut prendre la valeur
 * sorties : aucune
 * retour : valeur aléatoire produite
 * pré : i_min < i_max
 * post : le retour est entre i_min et i_max
 * précisions : la fonction est robuste si les assertions sont actives
 */

inline int genererValeur(int min, int max)
{
    unsigned int seed {(unsigned int) std::chrono::system_clock::now().time_since_epoch().count()};
    seed = 1; // à mettre en commentaires si on veux du vrai aléatoire
    
    static std::mt19937 generateur(seed);
    uint32_t valeur = generateur();
    
    double res = ((double(valeur)/generateur.max())*(max-min)) + min;
   
    return (int) res;
}


#endif
