/*
* fichier : jeu.h
* auteur.e : Vincent Ducharme
* date : 2016
* modifications :
* 01/11/2019 Marie-Flavie Auclair-Fortier : refactoring du code
* description : Ce fichier contient un jeu de serpent
*/

#ifndef jeu_h
#define jeu_h

/*
* description : gère la phase du jeu de serpent, créé la carte, le serpent et joue un tour à la fois
* entrées :
*   i_nbLignes, nombre de lignes dans le jeu
*   i_nbColonnes, nombre de colonnes dans le jeu
* pré :
*   i_nbLignes > 0
*   i_nbColonnes > 0
* précisions : le jeu devrait arrêter un jour, fonction non robuste si les assertions sont désactivées
*/

void jouer(unsigned int, unsigned int);

#endif /* jeu_h */
