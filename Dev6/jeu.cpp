/*
* fichier : jeu.cpp
* auteur.e : Vincent Ducharme
* date : 2016
* modifications :
* 01/11/2019 Marie-Flavie Auclair-Fortier : refactoring du code
* description : Ce fichier contient le code d'un jeu de serpent
*/

#include "fonctions.h"
#include "jeu.h"
#include "utilitaires.h"


/*
* description : gère la phase du jeu de serpent, créé la carte, le serpent et joue un tour à la fois
* entrées :
*   i_nbLignes, nombre de lignes dans le jeu
*   i_nbColonnes, nombre de colonnes dans le jeu
* pré :
*   i_nbLignes > 0
*   i_nbColonnes > 0
* précisions : le jeu devrait arrêter un jour, fonction non robuste si les assertions sont désactivées
*/

void jouer(unsigned int i_nbLignes, unsigned int i_nbColonnes)
{
    assert(i_nbLignes>0);
    assert(i_nbColonnes>0);

    // Prototypes des fonctions locales
    bool jouerUnTour(Carte &, Serpent &);
    
    // Declaration des variables locales
    bool jeuContinue {true};
    
    // 1. Creer la carte de jeu
    Carte carteDuJeu = creerCarte(i_nbLignes, i_nbColonnes);
    
    assert(carteDuJeu.m_nNbLig == i_nbLignes);
    assert(carteDuJeu.m_nNbCol == i_nbColonnes);

    // 2. Creer le serpent au milieu de la grille (définition)
    Serpent Ssssserpent = creerSerpent(i_nbLignes / 2, i_nbColonnes / 2);

    // 3. Assigner la position du serpent dans la carte
    assignerSerpent(carteDuJeu, Ssssserpent);
    //
    // 4. Tant qu'on ne veut pas arreter
    while (jeuContinue)
    {
        // 4.1 Jouer un tour
        jeuContinue = jouerUnTour(carteDuJeu, Ssssserpent);
    }
}

/*
* description : gère un tour du jeu de serpent
* entrées/sorties :
*   io_carte, carte du jeu
*   io_sssserpent, serpent du jeu
* pré :
* précisions : la mise à jour de la carte devrait conserver les préconditions vraies
*/

bool jouerUnTour(Carte &io_carte, Serpent &io_sssserpent)
{
    // Declaration des constantes locales
    const char QUITTER {'q'};
    
    // 1. Efface ce qu'il y a d'affiche a l'ecran (dans utilitaires.h)
    Console::ClearScreen();
    
    // 2. Affiche la carte du jeu
    cout << io_carte;
    cout.flush();
    
    // 3. Lit le prochain deplacement
    char cDirection(' ');
    cin >> cDirection;
    
    // 4. Si on ne veut pas quitter
    if (tolower(cDirection)  != QUITTER)
    {
        // 4.1 Recupere la nouvelle direction du serpent
        Direction dirSuivante = getDirectionSuivante(cDirection, io_sssserpent);
        
        // 4.2 Si le deplacement du serpent ne fonctionne pas
        if (!deplacer(dirSuivante, io_carte, io_sssserpent))
        {
            // 4.2.1 Affiche un message de fin de jeu avec le score (la longueur du serpent - la taille initiale)
            cout << "\t!!!Vous avez perdu! " << endl << "\t\tVotre pointage est : " << getLongueur(io_sssserpent) - TAILLE_INITIALE <<endl;
            return false;
        }
        // 4.3 Si le deplacement fonctionne
        else
        {
            // 4.3.1 Met a jour les informations sur la carte (module UpdateMap de la carte)
            mettreAJour(io_sssserpent, io_carte);
        }
    }
    // 5. Sinon, on demande d'arreter le jeu
    else
    {
        // 5.1 Affiche un message de fin de jeu avec le score (la longueur du serpent - la taille initiale)
        //cout << "\t!!!Vous avez quitté! " << endl << "\t\tVotre pointage est : " << getLongueur(io_sssserpent) - TAILLE_INITIALE<<endl;
        return false;
    }
    
    return true;

}
