//
//  fonctions.h
//  D6_Snake
//
//  Created by Marie-Flavie Auclair-Fortier on 2019-11-05.
//Copyright © 2019 Marie-Flavie Auclair-Fortier. All rights reserved.
//

#ifndef fonctions_h
#define fonctions_h

#include "types.h"

// Declaration de mes constantes
const int TAILLE_INITIALE = 3;
const int VIDE = 0;

/*
 * description : Fonction qui cree une carte
 * entrées : Nombres de lignes et le nombres de colonnes
 * sorties : carteDuJeu
 * pré : Le nombres de lignes et de colonnes doivent etre plus grands que 0.
 * post : La carte a les dimensions demandées
 * précisions : La fonction fait apparaitre la pomme une premiere fois
 */

Carte creerCarte(int i_nbLignes, int i_nbColonnes);

/*
 * description : Fonction qui cree un serpent
 * entrées : Le nombre de lignes dans le tableau du jeu et Le nombres de colonnes dans le tableau du jeu
 * sorties : Serpent
 * pré : Le nombres de lignes et de colonnes doivent être plus grands que 0.
 * post : Le serpent a une taille de 3 et la position correspond au milieu de la carte de jeu.
 * précisions :
 */

Serpent creerSerpent(int nbLignes, int  nbColonnes);

/*
 * description : Fonction qui cree la pomme du jeu
 * entrées : La carte
 * sorties : Une position pour la pomme
 * retour : aucun
 * pré : La carte dois être valide
 * post :
 * précisions :
 */

void creerPomme(Carte& i_carte);

/*
 * description : Fonction qui assigne le serpent dans la carte
 * entrées : Le nombre de lignes dans le tableau du jeu et Le nombres de colonnes dans le tableau du jeu
 * sorties : Carte du jeu avec le serpent dedans
 * pré : La carte dois etre valide
 * post : Le serpent dois etre dans la carte
 * précisions :
 */

void assignerSerpent(Carte& carteDuJeu, Serpent& Ssssserpent);

/*
 * description : Fonction qui transforme un caractere en direction pour le serpent
 * entrées : Le serpent et la direction
 * sorties : La direction que le seprent dois prendre
 * retour : Les directions
 * pré : Le serpent dois etre valide et les caracteres doivent etre ( q, w, a, s, d, )
 * post : Le serpent dois aller dans la direction demander
 * précisions :
 */

Direction getDirectionSuivante(char cDirection, Serpent& io_sssserpent);

/*
 * description : Fonction qui execute la direction demande dans getDirectionSuivante() et qui verifie si la position est valide
 * entrées : Une direction, la carte et le serpent
 * sorties : Le deplacement du serpent
 * retour : boolean
 * pré : La driection, la carte et le serpent doivent etre valide
 * post : Le serpent doit se deplacer dans la direction demande
 * précisions : Fait apparaitre les pommes apres que la premiere a ete mange
 */

bool deplacer(Direction dirSuivante, Carte& io_carte, Serpent& io_sssserpent);

/*
 * description : Fonction qui met a jour le serpent dans la carte
 * entrées : La carte et le serpent
 * sorties : La nouvelle position du serpent dans la carte
 * retour : Le serpent
 * pré : La carte et le serpent doivent etre valide
 * post : La position du serpent dois etre valide
 * précisions :
 */

Serpent mettreAJour(Serpent& io_sssserpent, Carte& io_carte);

/*
 * description : Fonction qui donne le score du jeu selon la taille du serpent
 * entrées : Le serpent
 * sorties : La taille du serpent
 * retour : La taille du serpent
 * pré : La taille du serpent dois etre valide
 * post : La taille dois etre la taille complete du serpent
 * précisions :
 */

int getLongueur(Serpent& io_sssserpent);

#endif /* fonctions_h */